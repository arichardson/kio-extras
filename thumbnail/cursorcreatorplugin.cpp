// SPDX-FileCopyrightText: 2021 Alexander Lohnau <alexander.lohnau@gmx.de>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <qplugin.h>
#include "macros.h"

#include "cursorcreator.h"

EXPORT_THUMBNAILER_WITH_JSON(CursorCreator, "cursorthumbnail.json")

#include "cursorcreatorplugin.moc"
